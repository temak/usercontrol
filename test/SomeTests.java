import static org.fest.assertions.Assertions.assertThat;

import models.IpAddressObtainer;

import org.junit.Test;

/**
 * 
 * @author Artem Sanakoev
 *
 */
public class SomeTests {

	@Test
	public void simpleCheck() {
		int a = 1 + 1;
		assertThat(a).isEqualTo(2);
	}
	//TODO this test need a Ebean
	//The default EbeanServer has not been defined? 
	//This is normally set via the ebean.datasource.default property. 
	//Otherwise it should be registered programatically via registerServer()
/*
	@Test
	public void chechIpAddressObtainer() {
		assertThat(IpAddressObtainer.obtainIpAddress(10, 20))
		.isEqualTo("10.162.10.20");
	}
	*/
}
