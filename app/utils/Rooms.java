package utils;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import play.i18n.Messages;

/**
 * 
 * @author Artem Sanakoev
 *
 */
public class Rooms {

	private static Map<Integer, String> options;

	static {
		LinkedHashMap<Integer, String> mapRooms = new LinkedHashMap<Integer, String>();
		mapRooms.put(1, "1 " + Messages.get("room"));
		mapRooms.put(20, "1a " + Messages.get("room"));
		for (int i = 2; i < 20; ++i) {
			mapRooms.put( i, "" + i + " " + Messages.get("room"));
		}
		options = Collections.unmodifiableMap(mapRooms);
	}

	public static Map<Integer, String> options() {
		return options;
	}
}
