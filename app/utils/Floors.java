package utils;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 
 * @author Artem Sanakoev
 *
 */
public class Floors {

	private static Map<String, String> options;

	static {
		LinkedHashMap<String, String> mapFloors = new LinkedHashMap<String, String>();
		for (int i = 1; i < 11; ++i) {
			mapFloors.put("" + i, "" + i + " этаж");
		}
		options = Collections.unmodifiableMap(mapFloors);
	}

	public static Map<String, String> options() {
		return options;
	}
}
