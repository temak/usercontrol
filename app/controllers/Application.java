package controllers;

import java.util.List;

import play.*;
import play.i18n.Messages;
import play.data.Form;
import play.mvc.*;

import views.html.*;
import models.IpAddressObtainer;
import models.User;

/**
 * 
 * @author Artem Sanakoev
 *
 */
public class Application extends Controller {

	/**
	 * This result directly redirect to application home.
	 */
	public static Result GO_HOME = redirect(routes.Users.register());

	public static Result index() {
		return ok(index.render("Hostel BSU #6"));
	}
	
	public static Result rules() {
		return ok(rules.render());
	}

}