package controllers;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import controllers.routes;
import exception.DevicesLimitReachedException;
import exception.NoSuchIPAddressException;

import models.HardwareAddressObtainer;
import models.IpAddressObtainer;
import models.PDFCreator;
import models.User;
import play.Logger;
import play.data.Form;
import play.i18n.Messages;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.register;
import views.html.registrationComplete;
import static utils.Debugger.DEV_MODE;

/**
 * 
 * @author Artem Sanakoev
 * 
 */
public class Users extends Controller {

	public static Result register() {

		Form<User> userForm = form(User.class);
		if (DEV_MODE) {
			System.out.println("DEV_MODE");
			userForm = userForm.fill(new User("Пётрик", "Бирюков",
					"petr@gmail.tut.by", "Таджикистонама", "ГумФак", 3, 8, 4,
					13));
		}
		return ok(register.render(userForm));
	}

	public static Result addNewUser() {
		if (DEV_MODE) {
			Logger.info("addNewUser() ");
		}
		Form<User> filledForm = form(User.class).bindFromRequest();
		if (Boolean.parseBoolean(filledForm.field("acceptRules").value()) != true) {
			Logger.info("not accepted rules");
			filledForm.reject("acceptRules", "please accept rules");
		}

		if (filledForm.hasErrors()) {
			Logger.info("validation errors");
			return badRequest(register.render(filledForm));
		}
		User user = filledForm.get();
		user.setRegistrationDate(new Date());
		System.out.println("DDSD");
		try {
			user.setIpAddress(IpAddressObtainer.obtain(user.getFloor(),
					user.getRoom()));

			user.setHardwareAddress(HardwareAddressObtainer.obtain(request()
					.remoteAddress()));
			PDFCreator creator = new PDFCreator(user);
			creator.create();
			flash("success", Messages.get("successRegistration"));

		} catch (IOException e) {
			flash("fail", Messages.get("errorOccurred"));
			return badRequest(register.render(filledForm));
		} catch (InterruptedException e) {
			flash("fail", Messages.get("errorOccurred"));
			return badRequest(register.render(filledForm));
		} catch (DevicesLimitReachedException e) {
			flash("fail", e.getMessage());
			return badRequest(register.render(filledForm));
		} catch (NoSuchIPAddressException e) {
			flash("fail", Messages.get("errorOccurred"));
			return badRequest(register.render(filledForm));
		}

		user.save();
		Logger.info("user " + user.id + " saved!");
		System.out.println(user);

		return redirect(routes.Users.registrationComplete(user.id));
	}

	public static Result registrationComplete(Long id) {
		User user = User.find.byId(id);
		// TODO
		// give user a *.pdf file to download
		return ok(registrationComplete.render(user));
	}

	public static Result statement(Long id) {
		return ok(new File("public/statements/statement_" + id + ".pdf"));
	}

	public static Result show(Long id) {
		return TODO;
	}
}
