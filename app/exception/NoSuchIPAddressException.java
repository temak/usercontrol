package exception;

/**
 * 
 * @author Artem Sanakoev
 *
 */
public class NoSuchIPAddressException extends Exception {

	private static final long serialVersionUID = -4510114720122238124L;

	public NoSuchIPAddressException() {
		super("Unable to obtain your hardware address. Please try again.");
	}

	public NoSuchIPAddressException(String message) {
		super(message);
	}
}
