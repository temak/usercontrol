package exception;

import play.i18n.Messages;

/**
 * 
 * @author Artem Sanakoev
 *
 */
public class DevicesLimitReachedException extends Exception {

	private static final long serialVersionUID = -6953060916209711439L;

	public DevicesLimitReachedException() {
		super(Messages.get("devicesLimitReached"));
	}

	public DevicesLimitReachedException(String message) {
		super(message);
	}

}
