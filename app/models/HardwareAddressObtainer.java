package models;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import static utils.Debugger.DEV_MODE;

import exception.NoSuchIPAddressException;

/**
 * 
 * @author Artem Sanakoev
 * 
 */
public class HardwareAddressObtainer {
	static int ADDRESS_COLUMN = 0;
	static int HWADDRESS_COLUMN = 2;

	/**
	 * 
	 * @return HardwareAdrress of the host with ipAddress
	 * @throws IOException
	 * @throws NoSuchIPAddressException
	 * 
	 *             Execs arp command and parses it's output for appropriate host
	 * @throws InterruptedException
	 * 
	 */
	public static String obtain(String ipAddress) throws IOException,
			NoSuchIPAddressException, InterruptedException {
		Process arpProcess = Runtime.getRuntime().exec(
				new String[] { "/bin/bash", "-c", "arp" });
		int exitValue = arpProcess.waitFor();
		BufferedReader arpBuffer = new BufferedReader(new InputStreamReader(
				arpProcess.getInputStream()));
		if (arpBuffer.ready()) {
			arpBuffer.readLine();
			while (true) {
				String line = arpBuffer.readLine();
				if (line == null) {
					throw new NoSuchIPAddressException();
				}
				String tokens[] = line.split(" +");
				if (tokens[ADDRESS_COLUMN].equals(ipAddress)) {
					return tokens[HWADDRESS_COLUMN];
				}
			}
		}
		return null;
	}
}
