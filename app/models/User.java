package models;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import play.data.format.*;
import play.data.validation.Constraints;
import play.db.ebean.Model;

/**
 * 
 * @author Artem Sanakoev
 * 
 */
@SuppressWarnings("serial")
@Entity
public class User extends Model {

	/**
	 * 
	 */

	@Id
	public Long id;

	@Constraints.Required
	private String surname;

	@Constraints.Required
	private String name;

	private String middlename;

	@Constraints.Email
	@Constraints.Required
	private String email;

	/**
	 * Just for fun and statistics
	 */
	@Constraints.Required
	private String country;

	@Constraints.Required
	private String faculty;

	/**
	 * year of the studying
	 */
	@Constraints.Required
	private int year;

	@Constraints.Required
	private int groupNumber;

	@Constraints.Required
	private int floor;

	/**
	 * 20 means 1a
	 */
	@Constraints.Required
	private int room;

	// @Constraints.Required
	private String ipAddress;

	// @Constraints.Required
	// @Column(unique = true)
	private String hardwareAddress;

	@Formats.DateTime(pattern = "yyyy-MM-dd")
	private Date registrationDate;

	// login
	// password

	enum Status {
		UNACTIVATED, ACTIVATED,
	}

	private Status status = Status.ACTIVATED;

	public static Finder<Long, User> find = new Finder<Long, User>(Long.class,
			User.class);

	public User(String surname, String name, String email, String country,
			String faculty, int year, int groupNumber, int floor, int room) {
		this.surname = surname;
		this.name = name;
		this.email = email;
		this.country = country;
		this.faculty = faculty;
		this.year = year;
		this.groupNumber = groupNumber;
		this.floor = floor;
		this.room = room;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMiddlename() {
		return middlename != null ? middlename : null;
	}

	public void setMiddlename(String middlename) {
		this.middlename = middlename;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getFaculty() {
		return faculty;
	}

	public void setFaculty(String faculty) {
		this.faculty = faculty;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getGroupNumber() {
		return groupNumber;
	}

	public void setGroupNumber(int groupNumber) {
		this.groupNumber = groupNumber;
	}

	public int getFloor() {
		return floor;
	}

	public void setFloor(int floor) {
		this.floor = floor;
	}

	public int getRoom() {
		return room;
	}

	public void setRoom(int room) {
		this.room = room;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getHardwareAddress() {
		return hardwareAddress;
	}

	public void setHardwareAddress(String string) {
		this.hardwareAddress = string;
	}

	public Date getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(Date registrationDate) {
		this.registrationDate = registrationDate;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String toString() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		return "User "
				+ id
				+ ":\n"
				+ "surname: "
				+ surname
				+ "\nname: "
				+ name
				+ "\nmiddlename: "
				+ middlename
				+ "\nemail: "
				+ email
				+ "\ncountry: "
				+ country
				+ "\nfaculty: "
				+ faculty
				+ "\nyear: "
				+ year
				+ "\nfloor: "
				+ floor
				+ "\nroom: "
				+ room
				+ "\nip Address: "
				+ ipAddress
				+ "\nHardware Address: "
				+ hardwareAddress
				+ "\nRegistratinDate: "
				+ dateFormat.format(registrationDate)
				+ "\nstatus: "
				+ ((status == Status.UNACTIVATED) ? "UNACTIVATED" : "ACTIVATED");
	}
}
