package models;

import static utils.Debugger.DEV_MODE;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

import play.Logger;

public class PDFCreator {

	private User user;
	private String statementSourcePath;

	public PDFCreator(User user) {
		this.user = user;
		statementSourcePath = String.format(
				"./public/statements/statement_%d.tex", user.id);
	}

	public void create() throws IOException, InterruptedException {
		parser("./resources/statement_template");
		execPdflatexCommand();
	}

	private void execPdflatexCommand() throws IOException, InterruptedException {
		if (DEV_MODE) {
			Logger.info("execShellCmd(String cmd) ");
		}
		Runtime runtime = Runtime.getRuntime();
		String pdflatexCommand = "pdflatex -output-directory ./public/statements "
				+ statementSourcePath;
		Process latexpdfProcess = runtime.exec(new String[] { "/bin/bash",
				"-c", pdflatexCommand });
		int exitValue = latexpdfProcess.waitFor();
		if (DEV_MODE) {
			Logger.info("exit value of latex: " + exitValue);
		}

		/*
		 * clean up temporary files after latexpdf
		 * 
		 * TODO remove all files with rm tool. And remove *.tex
		 */
		Process cleanProcess = runtime.exec(new String[] { "/bin/bash", "-c",
				"cd ./public/statements && latexmk -c" });
		exitValue = cleanProcess.waitFor();
		if (DEV_MODE) {
			Logger.info("exit value of latexmk -c: " + exitValue);
		}

		/*
		 * // just for debugging BufferedReader buf = new BufferedReader(new
		 * InputStreamReader(process.getInputStream())); String line = ""; while
		 * ((line = buf.readLine()) != null) { Logger.info("exec response: " +
		 * line); }
		 */
	}

	private void parser(String filename) throws IOException {
		FileReader statementTemplate = new FileReader(filename);

		FileWriter statementSource = new FileWriter(statementSourcePath);

		Scanner in = new Scanner(statementTemplate);
		String line = "";
		while (in.hasNext()) {
			line = in.nextLine();
			String tmp = "";
			boolean flag = false;
			int startOfTag = 0, endPrevTag = 0;
			for (int i = 0; i < line.length(); i++) {
				if (line.charAt(i) == '@') {
					if (flag) {
						tmp += resolveTag(line.substring(startOfTag, i + 1));
						endPrevTag = i + 1;
					} else {
						tmp += line.substring(endPrevTag, i);
						startOfTag = i;
					}
					flag = !flag;
				}
			}
			tmp += line.substring(endPrevTag, line.length());
			line = tmp;
			statementSource.write(line + "\n");
		}
		in.close();
		statementTemplate.close();
		statementSource.close();
	}

	private String resolveTag(String s) {
		if (s.equals("@name@")) {
			return user.getSurname() + " " + user.getName() + " "
					+ user.getMiddlename();
		} else if (s.equals("@room@")) {
			String room;
			if (user.getRoom() == 20) {
				room = "01a";
			} else {
				room = "" + user.getRoom();
			}
			return "" + user.getFloor() + room;
		} else if (s.equals("@faculty@")) {
			return user.getFaculty();
		} else if (s.equals("@year@")) {
			return "" + user.getYear();
		} else if (s.equals("@group@")) {
			return "" + user.getGroupNumber();
		} else if (s.equals("@data@")) {
			Date date = new Date();
			SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
			return formatter.format(date);
		}

		return "\\underline{\\hspace{5pt}      \\hspace{5pt}}";
	}
}
