package models;

import java.util.List;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Query;

import exception.DevicesLimitReachedException;

/**
 * 
 * @author Artem Sanakoev
 * 
 *         10.162.**.%room%0 reserved for WiFi AP
 */
public class IpAddressObtainer {
	final static String SUBNET_PART_ADDRESS = "10.162.";

	public static String obtain(int floor, int room)
			throws DevicesLimitReachedException {

		// find all users with specified room and floor
		String oql = " where floor = :custFloor and room = :custRoom ";
		Query<User> query = Ebean.createQuery(User.class, oql);
		query.setParameter("custFloor", floor);
		query.setParameter("custRoom", room);

		List<User> userList = query.findList();
		if (userList.size() > 8) {
			// means that there are already 9 devices in the room.
			throw new DevicesLimitReachedException();
		} else {
			return String.format("%s%d.%d%d", SUBNET_PART_ADDRESS, floor, room,
					userList.size() + 1);
		}
	}

	public static void test() throws DevicesLimitReachedException {
		System.out.println("============================");
		System.out.println("obtainIpAddress");
		System.out.println(obtain(103, 1));
		System.out.println(obtain(2, 20));
	}
}
