# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table user (
  id                        bigint not null,
  surname                   varchar(255),
  name                      varchar(255),
  middlename                varchar(255),
  email                     varchar(255),
  country                   varchar(255),
  faculty                   varchar(255),
  year                      integer,
  group_number              integer,
  floor                     integer,
  room                      integer,
  ip_address                varchar(255),
  hardware_address          varchar(255),
  registration_date         timestamp,
  status                    integer,
  constraint ck_user_status check (status in (0,1)),
  constraint pk_user primary key (id))
;

create sequence user_seq;




# --- !Downs

SET REFERENTIAL_INTEGRITY FALSE;

drop table if exists user;

SET REFERENTIAL_INTEGRITY TRUE;

drop sequence if exists user_seq;

