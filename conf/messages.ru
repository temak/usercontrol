surname = Фамилия
name = Имя
middlename = Отчество
country = Страна
faculty = Факультет
groupNumber = Группа
year = Курс
floor = Этаж
chooseFloor = -- Выберите этаж --
chooseRoom = -- Выберите комнату --
room = Комната

constraint.required = Обязательно

register = Зарегистрироваться

successRegistration = Вы успешно зарегистрировались!
downloadStatement = Скачать заявление
acceptRules = Я согласен(на) с правилами пользования сетью общежития.

rules = Правила работы в сети общежития

errorOccurred = Произошла ошибка в процессе регистрации. Пожалуйста попробуйте ещё раз.
devicesLimitReached = Вы исчерпали лимит сетевых устройств для вашей комнаты. Пожалуйста обратитесь к администратору.